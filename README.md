# Commit Log

**! Still under development !**

CommitLog is a small web app to check the commits of your favorite github repository. It is based on the quasar framework and utilizes highlight js and web workers.

## Future Features
- add night mode
- BitBucket and Gitlab support
- Smoother mobile version (I hoped that the web worker would have more impact here)

## Want to contribute?

``` bash
# checkout the branch and cd into the dir of the branch
git clone git@gitlab.com:nik.zantner/commitlog.git && cd commitlog

# install dependencies
npm install

# serve with hot reload at localhost:8080
quasar dev

# all in one
git clone git@gitlab.com:nik.zantner/commitlog.git && cd commitlog && npm install && quasar dev

```
