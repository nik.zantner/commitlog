export default [
  {
    path: '/',
    component: () => import('components/Index'),
    redirect: '/commits',
    children: [
      {
        path: '/commits',
        component: () => import('components/commits/CommitList'),
      },
      {
        path: '/legal',
        component: () => import('components/legal/Legal'),
      },
    ],
  },
  { path: '*', component: () => import('components/common/Error404') },
];
