import SingletonFactoryService from './SingletonFactory';

class HighlightService {
  constructor() {
    this.codeToHighlightQueue = [];
    this.worker = new Worker('./worker.js');
  }

  getHighlightedCode(code, callback) {
    this.handleInitialHighlight(code, callback);
  }

  handleInitialHighlight(code, callback) {
    if (!this.codeToHighlightQueue.length) {
      this.addCodeToHighlightQueue(code, callback);
      this.highlightNextCode();
    } else {
      this.addCodeToHighlightQueue(code, callback);
    }
  }

  // eslint-disable-next-line class-methods-use-this
  fixGithubAdditionsAndDeletions(code) {
    if (code.includes('hljs-addition') || code.includes('hljs-deletion')) {
      return code;
    }

    const lines = code.split('\n');
    const fixedLines = lines.map((line) => {
      if (line.substring(0, 1) === '+') {
        return `<span class="hljs-addition">${line}</span>`;
      } else if (line.substring(0, 1) === '-') {
        return `<span class="hljs-deletion">${line}</span>`;
      }
      return line;
    });

    return fixedLines.join('\n');
  }

  highlightNextCode() {
    if (this.codeToHighlightQueue.length) {
      const nextCodeWrapper = this.codeToHighlightQueue[0];
      this.worker.onmessage = (event) => {
        const highlightedCode = this.fixGithubAdditionsAndDeletions(event.data);
        nextCodeWrapper.callback(highlightedCode);
        this.codeToHighlightQueue.shift();
        this.highlightNextCode();
      };
      this.worker.postMessage(nextCodeWrapper.code);
    }
  }

  addCodeToHighlightQueue(code, callback) {
    this.codeToHighlightQueue.push({ code, callback });
  }
}

export default SingletonFactoryService.createFrozenSingleton(HighlightService);
