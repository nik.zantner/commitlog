function createSingleton(ClassToSingleton) {
  return new ClassToSingleton();
}

function createFrozenSingleton(ClassToSingleton) {
  const classToSingletonInstance = new ClassToSingleton();
  Object.freeze(classToSingletonInstance);
  return classToSingletonInstance;
}

const SingletonFactoryService = {
  createSingleton,
  createFrozenSingleton,
};

export default SingletonFactoryService;
