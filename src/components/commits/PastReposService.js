import SingletonFactoryService from '../common/SingletonFactory';

class PastReposService {
  constructor() {
    this.exampleRepos = ['quasarframework/quasar', 'torvalds/linux'];
  }

  addRepo(repo) {
    const repositories = localStorage.repositories ? JSON.parse(localStorage.repositories) : [];

    if (!repositories.includes(repo)) {
      repositories.push(repo);
      localStorage.repositories = JSON.stringify(repositories);
    }
  }

  getPastRepos() {
    return localStorage.repositories ? JSON.parse(localStorage.repositories) : this.exampleRepos;
  }
}

export default SingletonFactoryService.createFrozenSingleton(PastReposService);
