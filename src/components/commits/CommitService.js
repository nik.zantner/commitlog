import GitHub from 'github-api';
import SingletonFactoryService from '../common/SingletonFactory';
import { Notify } from 'quasar';

class CommitService {
  constructor() {
    this.gh = this.createGithubService();
    this.rateLimitSubscribers = [];
    this.repository = {};
  }

  searchRepository(query) {
    return this.gh.search(query).forRepositories().then(response => response.data);
  }

  createGithubService(username, password) {
    if (localStorage.loginData) {
      const loginData = this.getLoginData();
      return new GitHub({ username: loginData.username, password: loginData.password });
    }

    return new GitHub({ username, password });
  }

  isUserLoggedIn() {
    // eslint-disable-next-line no-underscore-dangle
    return !!this.gh.getUser().__authorizationHeader;
  }

  getUserProfile() {
    return this.gh.getUser().getProfile().then(response => response.data);
  }

  getLoginData() {
    if (localStorage.loginData) {
      return JSON.parse(localStorage.loginData);
    }

    return { username: undefined, password: undefined };
  }

  setRepository(repoIdentifier) {
    if (!repoIdentifier) {
      Notify.create('Set a repoistory identifier, like "quasarframework/quasar"');
      return;
    }

    const [userName, repoName] = repoIdentifier.split('/');
    this.repository = this.gh.getRepo(userName, repoName);
    this.storeLatestRepo(repoIdentifier);
  }

  storeLatestRepo(repoIdentifier) {
    localStorage.latestRepo = repoIdentifier;
  }

  loadLatestRepo() {
    return localStorage.latestRepo;
  }

  login(username, password, shouldStoreLoginData) {
    if (shouldStoreLoginData) {
      localStorage.loginData = JSON.stringify({ password, username });
    }

    this.gh = this.createGithubService(username, password);
  }

  listBranches() {
    return this.repository.listBranches().then(response => response.data);
  }

  getRepositoryDetails() {
    return this.repository.getDetails().then(response => response.data);
  }

  getCommits(selectedBranch, from, to) {
    return this.repository.listCommits({ sha: selectedBranch, since: to, until: from }).then((response) => {
      this.updateRateLimitSubscribers(response.headers['x-ratelimit-limit'], response.headers['x-ratelimit-remaining']);
      return response.data;
    });
  }

  getCommit(sha) {
    return this.repository.getSingleCommit(sha).then(response => response.data);
  }

  subscribeToRateLimitUpdates(callback) {
    this.rateLimitSubscribers.push(callback);
  }

  updateRateLimitSubscribers(totalRateLimit, remainingRateLimit) {
    this.rateLimitSubscribers.forEach(callback => callback(totalRateLimit, remainingRateLimit));
  }
}

export default SingletonFactoryService.createSingleton(CommitService);
