import moment from 'moment/moment';

function addFormatToHour(Vue) {
  Vue.filter('formatToHour', (value) => {
    if (!value) {
      throw Error('FormatDatePipe: You have to supply a date to format!');
    }

    return moment(String(value)).format('HH:MM');
  });
}

function addFormatToDate(Vue) {
  Vue.filter('formatToDate', (value) => {
    if (!value) {
      throw Error('FormatDatePipe: You have to supply a date to format!');
    }

    return moment(String(value)).format('DD.MM.YYYY');
  });
}

function addFormatToDateAndHour(Vue) {
  Vue.filter('formatToDateAndHour', (value) => {
    if (!value) {
      throw Error('FormatDatePipe: You have to supply a date to format!');
    }

    return moment(String(value)).format('DD.MM.YYYY HH:MM');
  });
}

export default ({ app, router, Vue }) => {
  addFormatToHour(Vue);
  addFormatToDate(Vue);
  addFormatToDateAndHour(Vue);
};
